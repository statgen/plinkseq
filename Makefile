
SYS = LINUX
#SYS = MAC

include ./config_defs.Makefile

MAKE_NAME =plinkseq-0.10

#######################
#Source files
#######################

#executable [main() function] files:
EXECS_SUB_DIR =execs
EXECS_SRC_DIR =$(SRC_DIR)/$(EXECS_SUB_DIR)
EXECS_SRC =$(wildcard $(EXECS_SRC_DIR)/*.cpp)

TARGETS =$(subst $(SRC_DIR),$(BLD_DIR),$(EXECS_SRC:.cpp=))
TARGETS_NO_DIR =$(notdir $(TARGETS))
TARGETS_SOFT_LINKS =$(addprefix ./, $(TARGETS_NO_DIR))

BLD_LIB_DIR =$(BLD_DIR)/$(LIB_SUB_DIR)
BLD_EXECS_DIR =$(BLD_DIR)/$(EXECS_SUB_DIR)

ALL_BLD_DIRS += $(BLD_EXECS_DIR)

SRC_DEPENDS += $(EXECS_SRC)

REMOVES += $(TARGETS_SOFT_LINKS)

# Files to be included when making the 'zip' file:
ZIP_INCLUDES = config_rules.Makefile config_defs.Makefile 


########################
#Static Libraries
########################

# protobuf
PROTOBUF_LIB_BASE_DIR =./sources/ext
PROTOBUF_LIB_DIR=$(PROTOBUF_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
PROTOBUF_LIBS =libprotobuf.a
PROTOBUF_INC =$(PROTOBUF_LIB_BASE_DIR)/$(INC_DIR)/
PROTOBUF_LIB_FULL_PATH =$(PROTOBUF_LIB_DIR)$(PROTOBUF_LIBS)

# plinkseq library:
PLINKSEQ_LIB_BASE_DIR =./sources/plinkseq
PLINKSEQ_LIB_DIR =$(PLINKSEQ_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
PLINKSEQ_LIBS =libplinkseq.a
PLINKSEQ_INC =$(PLINKSEQ_LIB_BASE_DIR)/$(INC_DIR)/
PLINKSEQ_LIB_FULL_PATH =$(PLINKSEQ_LIB_DIR)$(PLINKSEQ_LIBS)

# pseq library
PSEQ_LIB_BASE_DIR =./sources/pseq
PSEQ_LIB_DIR =$(PSEQ_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
PSEQ_LIBS =libpseq.a
PSEQ_INC =$(PSEQ_LIB_BASE_DIR)/$(INC_DIR)/
PSEQ_LIB_FULL_PATH =$(PSEQ_LIB_DIR)$(PSEQ_LIBS)

# util library
UTIL_LIB_BASE_DIR =./sources/util
UTIL_LIB_DIR =$(UTIL_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
UTIL_LIBS =libutil.a
UTIL_INC =$(UTIL_LIB_BASE_DIR)/$(INC_DIR)/
UTIL_LIB_FULL_PATH =$(UTIL_LIB_DIR)$(UTIL_LIBS)

# mongoose library
MONGOOSE_LIB_BASE_DIR =./sources/mongoose
MONGOOSE_LIB_DIR =$(MONGOOSE_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
MONGOOSE_LIBS =libmongoose.a
MONGOOSE_INC =$(MONGOOSE_LIB_BASE_DIR)/$(INC_DIR)/
MONGOOSE_LIB_FULL_PATH =$(MONGOOSE_LIB_DIR)$(MONGOOSE_LIBS)

# browser library
BROWSER_LIB_BASE_DIR =./sources/browser
BROWSER_LIB_DIR =$(BROWSER_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
BROWSER_LIBS =libbrowser.a
BROWSER_INC =$(BROWSER_LIB_BASE_DIR)/$(INC_DIR)/
BROWSER_LIB_FULL_PATH =$(BROWSER_LIB_DIR)$(BROWSER_LIBS)

# pdas library
PDAS_LIB_BASE_DIR =./sources/pdas
PDAS_LIB_DIR =$(PDAS_LIB_BASE_DIR)/$(BLD_LIB_DIR)/
PDAS_LIBS =libpdas.a
PDAS_INC =$(PDAS_LIB_BASE_DIR)/$(INC_DIR)/
PDAS_LIB_FULL_PATH =$(PDAS_LIB_DIR)$(PDAS_LIBS)

########################
#Executable dependencies (headers/libraries)
########################

#For example: -L./
LIB_LINK_DIRS =$(PLINKSEQ_LIB_DIR) $(PSEQ_LIB_DIR) $(UTIL_LIB_DIR) $(MONGOOSE_LIB_DIR) $(BROWSER_LIB_DIR) $(PDAS_LIB_DIR) $(PROTOBUF_LIB_DIR)
LIB_LINK_FLAGS =$(LIB_LINK_DIRS:%=-L%)

#For example: -I./
LIB_INCLUDE_DIRS += $(PLINKSEQ_INC) $(PSEQ_INC) $(UTIL_INC) $(MONGOOSE_INC) $(BROWSER_INC) $(PDAS_INC) $(PROTOBUF_INC)

#All directories with relevant libraries' makefiles
LIB_MAKE_DIRS =$(PLINKSEQ_LIB_BASE_DIR) $(PSEQ_LIB_BASE_DIR) $(UTIL_LIB_BASE_DIR) $(MONGOOSE_LIB_BASE_DIR) $(BROWSER_LIB_BASE_DIR) $(PDAS_LIB_BASE_DIR) $(PROTOBUF_LIB_BASE_DIR)


#For example: ./sources/hmm++/build/lib/libhmm++.a
PSEQ_EXEC_LIB_DEPENDS =$(PSEQ_LIB_FULL_PATH) $(PLINKSEQ_LIB_FULL_PATH) $(PROTOBUF_LIB_FULL_PATH)
PSEQ_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(PSEQ_EXEC_LIB_DEPENDS))
PSEQ_EXEC_LIB_LINKS =-lpthread -lz


#For example: ./sources/hmm++/build/lib/libhmm++.a
BEHEAD_EXEC_LIB_DEPENDS =
BEHEAD_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(BEHEAD_EXEC_LIB_DEPENDS))
BEHEAD_EXEC_LIB_LINKS =

# gcol
GCOL_EXEC_LIB_DEPENDS =
GCOL_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(GCOL_EXEC_LIB_DEPENDS))
GCOL_EXEC_LIB_LINKS =

# mm
MM_EXEC_LIB_DEPENDS =$(UTIL_LIB_FULL_PATH)
MM_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(MM_EXEC_LIB_DEPENDS))
MM_EXEC_LIB_LINKS =

# smp
SMP_EXEC_LIB_DEPENDS =$(UTIL_LIB_FULL_PATH) $(PLINKSEQ_LIB_FULL_PATH)  $(PROTOBUF_LIB_FULL_PATH)
SMP_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(SMP_EXEC_LIB_DEPENDS))
SMP_EXEC_LIB_LINKS =-lz -lpthread

# tab2vcf
TAB2VCF_EXEC_LIB_DEPENDS =$(UTIL_LIB_FULL_PATH) 
TAB2VCF_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(TAB2VCF_EXEC_LIB_DEPENDS))
TAB2VCF_EXEC_LIB_LINKS =

# mongoose
MONGOOSE_EXEC_LIB_DEPENDS = $(MONGOOSE_LIB_FULL_PATH)
MONGOOSE_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(MONGOOSE_EXEC_LIB_DEPENDS))
MONGOOSE_EXEC_LIB_LINKS =-lpthread -ldl

# browser
BROWSER_EXEC_LIB_DEPENDS = $(BROWSER_LIB_FULL_PATH) $(PLINKSEQ_LIB_FULL_PATH)  $(PROTOBUF_LIB_FULL_PATH)
BROWSER_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(BROWSER_EXEC_LIB_DEPENDS))
BROWSER_EXEC_LIB_LINKS =-lz -lpthread -ldl

# pdas
PDAS_EXEC_LIB_DEPENDS = $(PDAS_LIB_FULL_PATH) $(PLINKSEQ_LIB_FULL_PATH) $(PROTOBUF_LIB_FULL_PATH)
PDAS_EXEC_LIB_DEPENDS_NO_PATH =$(notdir $(PDAS_EXEC_LIB_DEPENDS))
PDAS_EXEC_LIB_LINKS =-lz -lpthread -ldl 

##########################################
#Compiler, Compilation flags:
##########################################

ALL_CXXFLAGS_LINKER =$(CXXFLAGS) $(STATIC_FLAG) $(LIB_LINK_FLAGS)

USE_DBGCXXFLAGS =DBGCXXFLAGS="$(DBGCXXFLAGS)" USE_DEBUG="$(USE_DEBUG)"

########################
#Rules
########################

#'make all' creates all dependencies, always calls make on libraries, and then on targets:
.DEFAULT_GOAL := all
.PHONY: all
all:
	$(MAKE) $(LIB_MAKE_DIRS)
	$(MAKE) $(TARGETS_SOFT_LINKS)

.PHONY: $(LIB_MAKE_DIRS)
$(LIB_MAKE_DIRS):
	$(MAKE) $(USE_DBGCXXFLAGS) -C $@


# The soft-link pointing to each of the executables listed in $(TARGETS):
$(TARGETS_SOFT_LINKS): %: $(BLD_EXECS_DIR)/%
		       ln -fs $< $@





#explicit rules for making executables:
$(BLD_DIR)/$(EXECS_SUB_DIR)/pseq: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(PSEQ_EXEC_LIB_DEPENDS)
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(PSEQ_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/behead: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(BEHEAD_EXEC_LIB_DEPENDS)
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(BEHEAD_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/gcol: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(GCOL_EXEC_LIB_DEPENDS)
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(GCOL_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/mm: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(MM_EXEC_LIB_DEPENDS)
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(MM_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/smp: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(SMP_EXEC_LIB_DEPENDS)
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(SMP_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/tab2vcf: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(TAB2VCF_EXEC_LIB_DEPENDS)
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(TAB2VCF_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/mongoose: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(MONGOOSE_EXEC_LIB_DEPENDS) 
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(MONGOOSE_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/pdas: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(PDAS_EXEC_LIB_DEPENDS) 
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(PDAS_EXEC_LIB_LINKS) -o $@

$(BLD_DIR)/$(EXECS_SUB_DIR)/browser: %: %.o $(ENSURE_BUILD_DIRS_PREREQS) $(BROWSER_EXEC_LIB_DEPENDS) 
	    $(CXX) $(ALL_CXXFLAGS_LINKER) $^ $(BROWSER_EXEC_LIB_LINKS) -o $@

# Clean-up rules:
clean: $(addsuffix .clean,$(LIB_MAKE_DIRS))
.PHONY: %.clean
%.clean:
	$(MAKE) -C $* clean


include ./config_rules.Makefile
