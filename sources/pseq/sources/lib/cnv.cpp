#include "cnv.h"
#include "util.h"

#include <iostream>
#include <set>
#include <cmath>
#include <sstream>
#include <algorithm>
using namespace std;

extern GStore g;
extern Pseq::Util::Options args;


const string forbiddenChars = "\\/:?\"<>|";
bool isForbiddenChar(char c) {
  return forbiddenChars.find(c) != string::npos;
}

string replaceForbiddenChars(const string& str) {
	std::string newStr;
	std::insert_iterator<std::string> newStrIt(newStr, newStr.begin());
	std::remove_copy_if(str.begin(), str.end(), newStrIt, isForbiddenChar);
	return newStr;
}


Pseq::VarDB::SomeQualityEventOracle::SomeQualityEventOracle(const Variant& v, double SQthresh, double NQthresh)
: EventOracle(),
  // We assume the first is always the REF allele:
  _numAltAlleles(v.n_alleles() - 1),
  _altToSamples(_numAltAlleles) {

	const int numIndivs = v.size();
	vector<vector<EventStatus> > indivAltStatus(numIndivs);

	for (int indivInd = 0; indivInd < numIndivs; ++indivInd) {
		const Genotype& gt = v(indivInd);

		if (gt.meta.has_field("DSCVR") && gt.meta.get1_string("DSCVR") == "Y")
			_discoveredIndiv.insert(indivInd);

		indivAltStatus[indivInd] = vector<EventStatus>(_numAltAlleles, MISSING);
		vector<EventStatus>& indivStatuses = indivAltStatus[indivInd];

		if (!gt.meta.has_field("SQ") || !gt.meta.has_field("NQ"))
			continue;
		vector<double> SQs = gt.meta.get_double("SQ");
		vector<double> NQs = gt.meta.get_double("NQ");
		if (SQs.size() != _numAltAlleles || NQs.size() != _numAltAlleles)
			continue;

		for (int altInd = 0; altInd < _numAltAlleles; ++altInd) {
			EventStatus status = MISSING;

			bool highSQ = SQs[altInd] >= SQthresh;
			bool highNQ = NQs[altInd] >= NQthresh;
			if (highSQ && !highNQ)
				status = HAS_EVENT;
			else if (highNQ && !highSQ)
				status = DOES_NOT_HAVE_EVENT;

			indivStatuses[altInd] = status;
		}
	}

	// Copy over the calculations:
	for (int altInd = 0; altInd < _numAltAlleles; ++altInd) {
		_altToSamples[altInd] = SampleToStatus(numIndivs, MISSING);
		SampleToStatus& altStatuses = _altToSamples[altInd];
		for (int indivInd = 0; indivInd < numIndivs; ++indivInd) {
			altStatuses[indivInd] = indivAltStatus[indivInd][altInd];
		}
	}
}

Pseq::VarDB::SomeQualityEventOracle::~SomeQualityEventOracle() {
}

#define CLASS_ALLELE_HEADER \
		<< "#CLASS" \
		<< "\t" << "CHILD" \
		<< "\t" << "LOCUS" \
		<< "\t" << "CNV" \
		<< "\t" << "NUM_TARG" \
		<< "\t" << "PAR_WITH" \
		<< "\t" << "PAR_WITHOUT" \
		<< "\t" << "PAR_MISS" \
		<< "\t" << "CHILD_WITH" \
		<< "\t" << "CHILD_WITHOUT" \
		<< "\t" << "CHILD_MISS"

#define ALLELE_OUTPUT \
		<< "\t" << child->id() \
		<< "\t" << locStr \
		<< "\t" << altAlleleName \
		<< "\t" << numTargets \
		<< "\t" << parentLookup.numIndivWithEvent \
		<< "\t" << parentLookup.numIndivWithoutEvent \
		<< "\t" << parentLookup.numIndivWithMissing \
		<< "\t" << childLookup.numIndivWithEvent \
		<< "\t" << childLookup.numIndivWithoutEvent \
		<< "\t" << childLookup.numIndivWithMissing


#define INDIV_SUMMARY_HEADER \
		<< "#SUMMARY" \
		<< "\t" << "CHILD" \
\
		<< "\t" << "CHILD_CNV" \
		<< "\t" << "IN_PATERNAL" \
		<< "\t" << "IN_MATERNAL" \
		<< "\t" << "IN_PATERNAL_AND_MATERNAL" \
		<< "\t" << "MISSING_PATERNAL" \
		<< "\t" << "MISSING_MATERNAL" \
		<< "\t" << "DE_NOVO" \
\
		<< "\t" << "PATERNAL_TRANSMITTED" \
		<< "\t" << "PATERNAL_NON_TRANSMITTED" \
		<< "\t" << "PATERNAL_UNKNOWN" \
\
		<< "\t" << "MATERNAL_TRANSMITTED" \
		<< "\t" << "MATERNAL_NON_TRANSMITTED" \
		<< "\t" << "MATERNAL_UNKNOWN"

#define INDIV_OUTPUT \
		<< "\t" << child \
\
		<< "\t" << summ._childCNV \
		<< "\t" << summ._inPaternal \
		<< "\t" << summ._inMaternal \
		<< "\t" << summ._inPaternalAndMaternal \
		<< "\t" << summ._missingPaternal \
		<< "\t" << summ._missingMaternal \
		<< "\t" << summ._denovo \
\
		<< "\t" << summ._paternal_transmitted \
		<< "\t" << summ._paternal_non_transmitted \
		<< "\t" << summ._paternal_unknown \
\
		<< "\t" << summ._maternal_transmitted \
		<< "\t" << summ._maternal_non_transmitted \
		<< "\t" << summ._maternal_unknown


void Pseq::VarDB::f_cnv_denovo_scan(Variant& v, void* p) {
	Out& dnCNVout = Out::stream("denovo.cnv");

	AuxCNVdeNovoData* aux = static_cast<AuxCNVdeNovoData*>(p);
	const Inds& allParentInds = aux->allParentInds;
	const Inds& allChildrenInds = aux->allChildrenInds;
	map<string, ChildTransmissionSummaries>& childrenSummaries = aux->childrenSummaries;

	EventOracle* childEventOracle = new SomeQualityEventOracle(v, aux->minChildSQ, aux->minChildNQ);
	EventOracle* parentEventOracle = new SomeQualityEventOracle(v, aux->minParSQ, aux->minParNQ);
	if (childEventOracle->getNumAltAlleles() != parentEventOracle->getNumAltAlleles())
		Helper::halt("Number of of alternate alleles in children and parents evaluation SHOULD match for the same variant v!");
	int numAltAlleles = childEventOracle->getNumAltAlleles();

	//string locStr = v.name();
	string locStr = v.coordinate();

	const int numIndivs = v.size();
	for (int childInd = 0; childInd < numIndivs; ++childInd) {
		Individual* child = v.ind(childInd);
		Individual* pat = child->pat();
		Individual* mat = child->mat();

		// only consider individuals where both parents are present:
		if (pat == NULL || mat == NULL)
			continue;

		// TODO : there will be a better way to get the actual individual slot, but use for now...
		const int patInd = g.indmap.ind_n(pat->id());
		const int matInd = g.indmap.ind_n(mat->id());

		int childSampInd = v.ind_sample(childInd);
		if (childSampInd == 0 || v.ind_sample(patInd) != childSampInd || v.ind_sample(matInd) != childSampInd) {
			stringstream str;
			str << "Skipping child " << child->id() << " since it appears in more than one sample or in a different sample than its parents";
			plog.warn(str.str());
			continue;
		}

		//const SampleVariant& childSampVariant = v.sample_metainformation(childSampInd);
		vector<SampleVariant*> fileSamples = v.fsample(childSampInd);
		if (fileSamples.size() != 1)
			continue;
		const SampleVariant& childSampVariant = *(fileSamples[0]);

		int numTargets = -1;
		if (childSampVariant.meta.has_field("NUMT"))
			numTargets = childSampVariant.meta.get1_int("NUMT");
		else if (v.consensus.meta.has_field("NUMT")) // when only a single sample, then info is only in consensus!
			numTargets = v.consensus.meta.get1_int("NUMT");
		else
			continue;

		Inds childsParents;
		childsParents.insert(patInd);
		childsParents.insert(matInd);

		Inds childIndSet;
		childIndSet.insert(childInd);

		ChildTransmissionSummaries& childSummaries = childrenSummaries[child->id()];

		bool testForDeNovos = aux->allowDeNovoWithoutDiscoveryInChild || childEventOracle->discoveredInIndiv(childInd);
		bool testForPaternalTransmission = aux->allowTransmissionWithoutDiscoveryInParent || parentEventOracle->discoveredInIndiv(patInd);
		bool testForMaternalTransmission = aux->allowTransmissionWithoutDiscoveryInParent || parentEventOracle->discoveredInIndiv(matInd);

		for (int altInd = 0; altInd < numAltAlleles; ++altInd) {
			// Start from 2nd allele [since we assume the first is always the REF allele]:
			const int alleleInd = altInd + 1;
			const string altAlleleName = v.allele(alleleInd).name();
			ChildTransmissionSummary& childSummaryForType = childSummaries._cnvSummaryByType[altAlleleName];

			const vector<EventOracle::EventStatus>& childSampStatuses = *(childEventOracle->getEventStatus(altInd));
			const vector<EventOracle::EventStatus>& parentSampStatuses = *(parentEventOracle->getEventStatus(altInd));

			LookupEventCounts parentLookup = lookupEventInIndivs(allParentInds, childsParents, parentSampStatuses);
			LookupEventCounts childLookup = lookupEventInIndivs(allChildrenInds, childIndSet, childSampStatuses);

			if (testForDeNovos) {
				if (childSampStatuses[childInd] == EventOracle::HAS_EVENT) {
					++(childSummaryForType._childCNV);

					if (parentSampStatuses[patInd] == EventOracle::HAS_EVENT)
						++(childSummaryForType._inPaternal);

					if (parentSampStatuses[matInd] == EventOracle::HAS_EVENT)
						++(childSummaryForType._inMaternal);

					if (parentSampStatuses[patInd] == EventOracle::HAS_EVENT && parentSampStatuses[matInd] == EventOracle::HAS_EVENT)
						++(childSummaryForType._inPaternalAndMaternal);

					if (parentSampStatuses[patInd] == EventOracle::MISSING)
						++(childSummaryForType._missingPaternal);

					if (parentSampStatuses[matInd] == EventOracle::MISSING)
						++(childSummaryForType._missingMaternal);

					if (parentSampStatuses[patInd] == EventOracle::DOES_NOT_HAVE_EVENT && parentSampStatuses[matInd] == EventOracle::DOES_NOT_HAVE_EVENT) {
						//
						// TODO -- If !REQUIRE_DE_NOVO_DISCOVERY_IN_CHILD, then instead of outputting here, perhaps need to cache DENOVO output so that can output ONLY the maximal super-segments of de novo CNVs:
						//
						dnCNVout
						<< "DENOVO"
						ALLELE_OUTPUT
						<< "\n";

						++(childSummaryForType._denovo);
					}
				}
			}

			if (testForPaternalTransmission) {
				if (parentSampStatuses[patInd] == EventOracle::HAS_EVENT && parentSampStatuses[matInd] == EventOracle::DOES_NOT_HAVE_EVENT) {
					if (childSampStatuses[childInd] == EventOracle::MISSING) {
						dnCNVout
						<< "PATERNAL_UNKNOWN";
						++(childSummaryForType._paternal_unknown);
					}
					if (childSampStatuses[childInd] == EventOracle::HAS_EVENT) {
						dnCNVout
						<< "PATERNAL_TRANSMITTED";
						++(childSummaryForType._paternal_transmitted);
					}
					if (childSampStatuses[childInd] == EventOracle::DOES_NOT_HAVE_EVENT) {
						dnCNVout
						<< "PATERNAL_NON_TRANSMITTED";
						++(childSummaryForType._paternal_non_transmitted);
					}

					dnCNVout
					ALLELE_OUTPUT
					<< "\n";
				}
			}

			if (testForMaternalTransmission) {
				if (parentSampStatuses[matInd] == EventOracle::HAS_EVENT && parentSampStatuses[patInd] == EventOracle::DOES_NOT_HAVE_EVENT) {
					if (childSampStatuses[childInd] == EventOracle::MISSING) {
						dnCNVout
						<< "MATERNAL_UNKNOWN";
						++(childSummaryForType._maternal_unknown);
					}
					if (childSampStatuses[childInd] == EventOracle::HAS_EVENT) {
						dnCNVout
						<< "MATERNAL_TRANSMITTED";
						++(childSummaryForType._maternal_transmitted);
					}
					if (childSampStatuses[childInd] == EventOracle::DOES_NOT_HAVE_EVENT) {
						dnCNVout
						<< "MATERNAL_NON_TRANSMITTED";
						++(childSummaryForType._maternal_non_transmitted);
					}

					dnCNVout
					ALLELE_OUTPUT
					<< "\n";
				}
			}
		}
	}

	delete childEventOracle;
	delete parentEventOracle;
}

Pseq::VarDB::LookupEventCounts Pseq::VarDB::lookupEventInIndivs(const Inds& lookupInds, const Inds& excludeInds, const vector<EventOracle::EventStatus>& sampStatuses) {
	LookupEventCounts lookup;

	for (Inds::const_iterator indivIt = lookupInds.begin(); indivIt != lookupInds.end(); ++indivIt) {
		const int indiv = *indivIt;
		if (excludeInds.find(indiv) == excludeInds.end()) {
			if (sampStatuses[indiv] == EventOracle::HAS_EVENT)
				lookup.numIndivWithEvent++;
			else if (sampStatuses[indiv] == EventOracle::DOES_NOT_HAVE_EVENT)
				lookup.numIndivWithoutEvent++;
			else if (sampStatuses[indiv] == EventOracle::MISSING)
				lookup.numIndivWithMissing++;
		}
	}

	return lookup;
}

Pseq::VarDB::AuxCNVdeNovoData::AuxCNVdeNovoData(const Pseq::Util::Options& args)
: minChildSQ(0), minChildNQ(0), minParSQ(0), minParNQ(0), allowDeNovoWithoutDiscoveryInChild(false), allowTransmissionWithoutDiscoveryInParent(false) {

	SET_INT(minChildSQ)
	SET_INT(minChildNQ)
	SET_INT(minParSQ)
	SET_INT(minParNQ)

	int minSQ = 0;
	int minNQ = 0;
	SET_INT(minSQ)
	SET_INT(minNQ)

	// Ensure that minSQ is enforced:
	minChildSQ = max(minChildSQ, minSQ);
	minParSQ = max(minParSQ, minSQ);

	// Ensure that minNQ is enforced:
	minChildNQ = max(minChildNQ, minNQ);
	minParNQ = max(minParNQ, minNQ);

	SET_FLAG(allowDeNovoWithoutDiscoveryInChild)
	SET_FLAG(allowTransmissionWithoutDiscoveryInParent)
}

bool Pseq::VarDB::cnv_denovo_scan(Mask& mask) {
	Out& dnCNVout = Out::stream("denovo.cnv");
	Out& dnIndivOut = Out::stream("denovo.cnv.indiv");

	AuxCNVdeNovoData* aux = new AuxCNVdeNovoData(args);

	cerr << "Starting CNV de novo scan..." << endl;
	const int n = g.indmap.size();

	// Attach parents to children:
	for (int i = 0; i < n; ++i) {
		Individual* person = g.indmap(i);
		g.inddb.fetch(person);

		Individual* p = g.indmap.ind(person->father());
		Individual* m = g.indmap.ind(person->mother());

		if (p)
			person->pat(p);
		if (m)
			person->mat(m);

		// Add inds for child and for parents ONLY if individual i has both parents (i.e., is a child), and also initialize empty summary row for each such child:
		if (p && m) {
			aux->allChildrenInds.insert(i);

			aux->allParentInds.insert(g.indmap.ind_n(p->id()));
			aux->allParentInds.insert(g.indmap.ind_n(m->id()));

			aux->childrenSummaries[person->id()] = ChildTransmissionSummaries();
		}
	}

	dnCNVout
	CLASS_ALLELE_HEADER
	<< "\n";

	g.vardb.iterate(Pseq::VarDB::f_cnv_denovo_scan, aux, mask);

	dnIndivOut
	INDIV_SUMMARY_HEADER
	<< "\n";

	set<string> cnvTypesSet;
	list<string> cnvTypes;
	for (map<string, ChildTransmissionSummaries>::const_iterator it = aux->childrenSummaries.begin(); it != aux->childrenSummaries.end(); ++it) {
		const string& child = it->first;
		const ChildTransmissionSummaries& summaries = it->second;
		for (map<string, ChildTransmissionSummary>::const_iterator tIt = summaries._cnvSummaryByType.begin(); tIt != summaries._cnvSummaryByType.end(); ++tIt) {
			const string& type = tIt->first;
			if (cnvTypesSet.find(type) == cnvTypesSet.end()) {
				cnvTypesSet.insert(type);
				cnvTypes.push_back(type);
			}
		}
		ChildTransmissionSummary summ = summaries.getAllCNVsummary();

		dnIndivOut
		<< "<CNV>"
		INDIV_OUTPUT
		<< "\n";
	}

	for (list<string>::const_iterator tIt = cnvTypes.begin(); tIt != cnvTypes.end(); ++tIt) {
		const string& type = *tIt;
		string typeInFileName = replaceForbiddenChars(type);

		string typeFileSuffix = "denovo." + typeInFileName + "_cnv.indiv";
		Out typeOutput( typeFileSuffix , "per-trio output for " + type + " CNV from cnv-denovo" );
		Out& dnIndivTypeOut = Out::stream(typeFileSuffix);

		dnIndivTypeOut
		INDIV_SUMMARY_HEADER
		<< "\n";

		for (map<string, ChildTransmissionSummaries>::iterator it = aux->childrenSummaries.begin(); it != aux->childrenSummaries.end(); ++it) {
			const string& child = it->first;
			ChildTransmissionSummaries& summaries = it->second;
			const ChildTransmissionSummary& summ = summaries._cnvSummaryByType[type];

			dnIndivTypeOut
			<< type
			INDIV_OUTPUT
			<< "\n";
		}
	}

	delete aux;

	return true;
}
